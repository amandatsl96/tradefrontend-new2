import { Component } from '@angular/core';
import { AuthenticationService } from '../authentication-service/authentication.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-logbutton',
  templateUrl: './logbutton.component.html',
  styleUrls: ['./logbutton.component.css']
})
export class LogbuttonComponent {

  public status : number = STATUS.UNKNOWN;

  userId : Observable<string>;

  constructor(private as : AuthenticationService) { }

  queryAuthentication() : void {
    this.as.checkAuthenticated().subscribe(
      data => {
        this.status = STATUS.LOGGED_IN;
        console.log(this.status);
      },
      error => {
        this.status = STATUS.NOT_LOGGED_IN;
        console.log(this.status);
      }
    )
  }

  label() : string {
    if (this.status == STATUS.UNKNOWN) {
      return "";
    } else if (this.status == STATUS.LOGGED_IN) {
      return "You are logged in!";
    }
    return "Redirecting you to login page...";
  }


}

const STATUS = {
  UNKNOWN: 0,
  LOGGED_IN: 1,
  NOT_LOGGED_IN: 2
}