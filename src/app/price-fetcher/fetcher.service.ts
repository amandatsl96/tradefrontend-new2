import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URLs } from '../../environments/environment';
import { Observable } from 'rxjs';
import { Quote } from './quote';

@Injectable({
  providedIn: 'root'
})
export class FetcherService {
  private readonly url : string = URLs.dataService + '/real/';
  
  constructor(private http : HttpClient) {}

  fetch(ticker : string) : Observable<Quote> {
    return this.http.get<Quote>(this.url + ticker, {responseType: 'json'});
  }
}
