import { TestBed } from '@angular/core/testing';

import { GetTradesService } from './get-trades.service';

describe('GetTradesService', () => {
  let service: GetTradesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GetTradesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
