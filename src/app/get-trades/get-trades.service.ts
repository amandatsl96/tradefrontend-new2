import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Trade } from '../model/trade';
import { URLs } from '../../environments/environment';
import { AuthenticationService } from '../authentication-service/authentication.service';
import { TransitiveCompileNgModuleMetadata } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class GetTradesService {

  private readonly url : string = URLs.tradeService;

  constructor(private httpClient: HttpClient, private authentication : AuthenticationService) { }

  getHistory() : Observable<Array<Trade>> {
    let history = this.httpClient.get<Array<Trade>>(
      this.url + '/getHistory', {headers: this.authentication.authorisation, responseType: 'json'}
    );
    return history;
  }

  getHistoryByTicker(ticker : string) : Observable<Array<Trade>>{
    let history = this.httpClient.get<Array<Trade>> (
      this.url + '/getHistory?' + ticker, {headers: this.authentication.authorisation, responseType: 'json'}
    );
    return history;
  }

  getTickers() : Observable<Array<String>>{
    let history = this.httpClient.get<Array<String>> (
      this.url + '/getTickers', {headers: this.authentication.authorisation, responseType: 'json'}
    );
    return history;
  }

  getDummy() : Trade[] {
    let dummies = new Array<Trade>();
    dummies.push(new Trade('AAPL', '10', 'BUY'));
    dummies.push(new Trade('MSFT', '5', 'BUY'));
    dummies.push(new Trade('C', '30', 'BUY'));
    return dummies;
  }


}
