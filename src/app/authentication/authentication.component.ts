import { Component } from '@angular/core';
import { AuthenticationService } from '../authentication-service/authentication.service';
import { Credential } from '../authentication-service/auth-credential'
import { Router } from '@angular/router';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.css']
})
export class AuthenticationComponent {

  credentials = {username: '', password: ''};

  public error : boolean = false;
  public success : boolean = false;

  constructor(private as : AuthenticationService, private router : Router) { }

  login(email : string, password : string) : void {
    let c = new Credential(email, password);
    this.as.authenticate(c, () => {
      this.router.navigateByUrl('/');
    });
    this.error = !this.as.authenticated;
  }

}

