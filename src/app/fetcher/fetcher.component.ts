import { Component, OnInit, Input } from '@angular/core';
import { FetcherService } from '../price-fetcher/fetcher.service';
import { Observable } from 'rxjs';
import { Quote } from '../price-fetcher/quote';

@Component({
  selector: 'app-fetcher',
  templateUrl: './fetcher.component.html',
  styleUrls: ['./fetcher.component.css']
})
export class FetcherComponent {

  @Input() quote : Observable<Quote>;

}
