import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { URLs } from '../../environments/environment';
import { Observable } from 'rxjs';
import { Credential } from './auth-credential';
import { map } from 'rxjs/operators';
import { callbackify } from 'util';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  public authorisation : HttpHeaders = new HttpHeaders({});

  private readonly url : string = URLs.userService;

  private readonly loginPath : string = '/login';

  public authenticated : boolean = false;

  constructor(private http : HttpClient) { }

  public authenticate(credential : Credential, callback) {
    this.http.post(this.url + this.loginPath, credential, 
      {observe: 'body', responseType: 'text'}).subscribe(data => {
        this.constructApiHeader(data);
        this.authenticated = true;
        return callback && callback();
      }, error => this.authenticated = false);
  }

  public constructApiHeader(apiKey : string) : void {
    if (apiKey != undefined) {
      this.authorisation = new HttpHeaders({
        'x-api-key': apiKey
      });
    }
  }

  public checkAuthenticated() : Observable<string> {
    var api : string = '';
    if (this.authorisation.has('x-api-key')) {
      api = this.authorisation.get('x-api-key');
    }
    return this.http.get<string>(this.url + '/authenticate/' + api)
  }

}
